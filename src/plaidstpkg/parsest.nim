import std/lexbase

type
  TokKind = enum
    tkInvalid

  Token = object
    kind: TokKind
    literal: string

  StLexer* = object of BaseLexer
    filename: string

const
  reservedWords = @[
    # constants
    "nil", "false", "true",
    # pseudo-variables
    "self", "super", "thisContext"
  ]

proc close(L: var StLexer) =
  lexbase.close(L)

proc getColumn(L: StLexer): int =
  result = lexbase.getColNumber(L, L.bufpos)

proc getLine(L: StLexer): int =
  result = L.lineNumber

proc handleCRLF(c: var StLexer, pos: int): int =
  case c.buf[pos]
  of '\c': result = lexbase.handleCR(c, pos)
  of '\L': result = lexbase.handleLF(c, pos)
  else: result = pos

