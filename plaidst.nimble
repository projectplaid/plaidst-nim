# Package

version       = "0.0.1"
author        = "Robert Roland"
description   = "Plaid Smalltalk compiler"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["plaidst"]


# Dependencies

requires "nim >= 1.9.3"
